import splash from '../assets/logo.png';
import intro_one from '../assets/intro1.png';
import intro_two from '../assets/intro2.png';
import intro_three from '../assets/intro3.png';
import intro_four from '../assets/intro4.png';
import location from '../assets/location_permission.png';

export default {
  splash,
  intro_one,
  intro_two,
  intro_three,
  intro_four,
  location,
};
