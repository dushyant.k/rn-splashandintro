import {Message} from './message';

export const Routes = {
  Splash: Message.screen_splash,
  Dashboard: Message.screen_dashboard,
  RouteMain: Message.screen_route_main,
  SearchPlace: Message.screen_search_place,
  Settings: Message.screen_settings,
};
