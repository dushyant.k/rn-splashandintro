/* eslint-disable react-hooks/rules-of-hooks */
import React from 'react';
import {Platform, Dimensions} from 'react-native';
import {useHeaderHeight} from '@react-navigation/stack';

const {width, height} = Dimensions.get('window');

export const screenWidth = Dimensions.get('window').width;
export const screenHeight = Dimensions.get('window').height;

export const LANDSCAPE = 'landscape';
export const PORTRAIT = 'portrait';

export function isIOS() {
  return Platform.OS === 'ios' ? true : false;
}

// const isTablet = () => {
//     if (DeviceInfo.isTablet()) {
//         return true
//     } else {
//         return false
//     }
// };

const responsiveHeight = height => {
  if (!isTablet()) return height;
  else return height + height * 0.25;
};

const convertFontScale = fontSize => {
  const baseSize = Platform.select({android: 400, ios: 375});
  return fontSize * (screenWidth / baseSize);
};

// const getHeaderHeight = () => {
//     let height;
//     const orientation = getOrientation();
//     height = getHeaderSafeAreaHeight();
//     height += DeviceInfo.isIPhoneX_deprecated && orientation === PORTRAIT ? 24 : 0;

//     return height;
// };

export const getHeaderSafeAreaHeight = () => {
  const headerHeight = useHeaderHeight();

  const orientation = getOrientation();
  if (Platform.OS === 'ios' && orientation === LANDSCAPE && !Platform.isPad) {
    return 32;
  }
  return headerHeight;
};

export const getOrientation = () => {
  const {width, height} = Dimensions.get('window');
  return width > height ? LANDSCAPE : PORTRAIT;
};

export {convertFontScale, responsiveHeight, width, height};
