import {Platform} from 'react-native';

export const PopBold = 'Poppins-SemiBold';
export const PopLight = 'Poppins-Light';
export const PopMedium = 'Poppins-Medium';
export const PopRegular = 'Poppins-Regular';
export const PopSemiBold = 'Poppins-SemiBold';
