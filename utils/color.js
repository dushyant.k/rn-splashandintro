const Color = {
  WHITE: '#FFFFFF',
  BLACK: '#000000',
  TRANSPARENT: 'transparent',

  bgColor: [
    '#0074f9',
    '#9E9E9E4D',
    '#F83B00',
    '#fd363d',
    '#e5f1ff',
    '#707070',
    '#4BB543',
  ],
  textColor: [
    '#0074f9',
    '#344152',
    '#F83B00',
    '#737c86',
    '#1B1B1B',
    '#AAAAAA',
    '#000000',
    '#333333',
    '#4BB543',
  ],
  buttonBgColor: ['#0074f9'],
  borderColor: [
    '#0074f9',
    '#CCCCCC',
    '#0000001A',
    '#fd363d',
    '#F83B00',
    '#DEDEDE',
  ],
  iconColor: ['#C5C5C5', '#D6D6D6', '#fd363d', '#F83B00', '#0074f9', '#4BB543'],
};

export default Color;
