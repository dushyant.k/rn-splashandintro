/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */

import React from 'react';
import {
  SafeAreaView,
  Image,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import color from '../utils/color';
import {convertFontScale, screenWidth} from '../utils/theme';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {Message} from '../utils/message';
import {ScrollView} from 'react-native-gesture-handler';

const slides = [
  {
    id: '1',
    key: 'One',
    title: 'Route Planning',
    subtitle: 'Plan routes with multiple destinations in optimised way',
    image: require('../assets/intro1.png'),
  },
  {
    id: '2',
    key: 'Two',
    title: 'Route Estimation',
    subtitle:
      'Know exactly how long a route will take and how many miles you will drive',
    image: require('../assets/intro2.png'),
  },
  {
    id: '3',
    key: 'Three',
    title: 'History Management',
    subtitle: 'Manage history of all completed routes',
    image: require('../assets/intro3.png'),
  },
  {
    id: '4',
    key: 'Four',
    title: 'Save time and fuel',
    subtitle: 'Drivers save more time spent on the road and money on fuel',
    image: require('../assets/intro4.png'),
  },
];

const OnboardingScreen = props => {
  const [currentIndex, setCurrentIndex] = React.useState(1);
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: color.WHITE}}>
      <View style={{flex: 1}}>
        <View style={{height: heightPercentageToDP('68%')}}>
          <ScrollView
            style={{flex: 1}}
            horizontal={true}
            scrollEventThrottle={16}
            pagingEnabled={true}
            showsHorizontalScrollIndicator={false}
            onMomentumScrollEnd={event => {
              console.log(
                '' +
                  (Math.round(event.nativeEvent.contentOffset.x / screenWidth) +
                    1),
              );
              setCurrentIndex(
                Math.round(event.nativeEvent.contentOffset.x / screenWidth) + 1,
              );
            }}>
            {slides.map((obj, index) => {
              console.log('currentIndex ' + currentIndex + ' ' + obj);
              console.log('currentIndex ' + (currentIndex === obj));
              return (
                <View style={styles.container} key={index}>
                  <Image
                    style={{
                      width: 300,
                      height: 300,
                    }}
                    source={obj.image}
                  />

                  <Text
                    style={{
                      fontSize: convertFontScale(22),
                      color: color.BLACK,
                      fontWeight: 'bold',
                      alignSelf: 'center',
                      marginTop: convertFontScale(100),
                      opacity: 0.9,
                    }}>
                    {obj.title}
                  </Text>
                  <Text
                    style={{
                      fontSize: convertFontScale(15),
                      color: '#A8A8A8',
                      alignSelf: 'center',
                      marginTop: 10,
                    }}>
                    {obj.subtitle}
                  </Text>
                </View>
              );
            })}
          </ScrollView>
        </View>

        {/* page controller  */}
        <View
          style={{
            alignSelf: 'center',
            flexDirection: 'row',
            paddingHorizontal: 30,
            marginTop: 40,
          }}>
          <View
            style={{
              backgroundColor:
                currentIndex === 1 ? color.bgColor[0] : color.bgColor[1],
              height: 3,
              width:
                currentIndex === 1
                  ? convertFontScale(26)
                  : convertFontScale(20),
              borderRadius: convertFontScale(3),
            }}
          />
          <View
            style={{
              backgroundColor:
                currentIndex === 2 ? color.bgColor[0] : color.bgColor[1],
              height: convertFontScale(4),
              width:
                currentIndex === 2
                  ? convertFontScale(26)
                  : convertFontScale(20),
              borderRadius: convertFontScale(3),
              marginLeft: convertFontScale(6),
            }}
          />
          <View
            style={{
              backgroundColor:
                currentIndex === 3 ? color.bgColor[0] : color.bgColor[1],
              height: convertFontScale(4),
              width:
                currentIndex === 3
                  ? convertFontScale(26)
                  : convertFontScale(20),
              borderRadius: convertFontScale(3),
              marginLeft: convertFontScale(6),
            }}
          />
          <View
            style={{
              backgroundColor:
                currentIndex === 4 ? color.bgColor[0] : color.bgColor[1],
              height: convertFontScale(4),
              width:
                currentIndex === 4
                  ? convertFontScale(26)
                  : convertFontScale(20),
              borderRadius: convertFontScale(3),
              marginLeft: convertFontScale(6),
            }}
          />
        </View>
        {/* footer */}
      </View>

      <View
        style={{
          position: 'absolute',
          bottom: convertFontScale(50),
          alignItems: 'center',
          alignSelf: 'center',
          width: '100%',
        }}>
        <TouchableOpacity
          style={{
            alignItems: 'center',
            alignSelf: 'center',
            justifyContent: 'center',
            borderRadius: convertFontScale(6),
            width: widthPercentageToDP('80%'),
            height: convertFontScale(48),
            backgroundColor: color.bgColor[0],
          }}
          onPress={async () => {
            props.navigation.replace(Message.screen_location_perm);
          }}>
          <Text
            style={{
              fontSize: convertFontScale(15),
              color: color.WHITE,
              fontWeight: 'bold',
            }}>
            {Message.skip_intro}
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    width: screenWidth,
    height: heightPercentageToDP('60%'),
    paddingVertical: heightPercentageToDP('10%'),
    paddingHorizontal: convertFontScale(22),
    alignItems: 'center',
  },
});

// const styles = StyleSheet.create({
//   subtitle: {
//     color: COLORS.white,
//     fontSize: 13,
//     marginTop: 10,
//     maxWidth: '70%',
//     textAlign: 'center',
//     lineHeight: 23,
//   },
//   title: {
//     color: COLORS.white,
//     fontSize: 22,
//     fontWeight: 'bold',
//     marginTop: 20,
//     textAlign: 'center',
//   },
//   image: {
//     height: '100%',
//     width: '100%',
//     resizeMode: 'contain',
//   },
//   indicator: {
//     height: 2.5,
//     width: 10,
//     backgroundColor: 'grey',
//     marginHorizontal: 3,
//     borderRadius: 2,
//   },
//   btn: {
//     flex: 1,
//     height: 50,
//     borderRadius: 5,
//     backgroundColor: '#fff',
//     justifyContent: 'center',
//     alignItems: 'center',
//   },
// });
export default OnboardingScreen;
