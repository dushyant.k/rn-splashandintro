/* eslint-disable prettier/prettier */
// import React, {Component} from 'react';
// import {
//   Platform,
//   StyleSheet,
//   View,
//   Text,
//   Image,
//   Alert,
//   SafeAreaView,
// } from 'react-native';
// import {logo} from '../Constants/images';
// import {McText, McImage} from '../Components';
// import {Images} from '../Constants';
// import {Message} from '../utils/message';
// import {
//   heightPercentageToDP,
//   widthPercentageToDP,
// } from 'react-native-responsive-screen';
// import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
// import AppIntroSlider from 'react-native-app-intro-slider';
// import color from '../utils/color';
// import {convertFontScale, screenHeight, screenWidth} from '../utils/theme';

// const infoArr = [
//   {
//     key: 'One',
//     title: 'Route Planning',
//     description: 'Plan routes with multiple destinations in optimised way',
//     image: require('../assets/intro1.png'),
//   },
//   {
//     key: 'Two',
//     title: 'Route Estimation',
//     description:
//       'Know exactly how long a route will take and how many miles you will drive',
//     image: require('../assets/intro2.png'),
//   },
//   {
//     key: 'Three',
//     title: 'History Management',
//     description: 'Manage history of all completed routes',
//     image: require('../assets/intro3.png'),
//   },
//   {
//     key: 'Four',
//     title: 'Save time and fuel',
//     description: 'Drivers save more time spent on the road and money on fuel',
//     image: require('../assets/intro4.png'),
//   },
// ];

// export default class Myapp extends Component<{}> {
//   constructor() {
//     super();
//     this.state = {
//       isVisible: true,
//     };
//   }
//   Hide_Splash_Screen = () => {
//     this.setState({
//       isVisible: false,
//     });
//   };

//   componentDidMount() {
//     var that = this;
//     setTimeout(function () {
//       that.Hide_Splash_Screen();
//     }, 5000);
//   }

//   render(props) {
//     // const {navigation} = this.props;
//     let Splash_Screen = (
//       <View style={styles.SplashScreen_RootView}>
//         <View style={styles.SplashScreen_ChildView}>
//           <Image
//             source={{
//               uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1200px-React-icon.svg.png',
//             }}
//             style={{width: 100, height: 100, resizeMode: 'contain'}}
//           />
//         </View>
//       </View>
//     );
//     const [currentIndex, setCurrentIndex] = React.useState(1);
//     return (
//       <SafeAreaView style={{flex: 1, backgroundColor: color.WHITE}}>
//         <View style={{flex: 1}}>
//           <View style={{height: heightPercentageToDP('68%')}}>
//             <ScrollView
//               style={{flex: 1}}
//               horizontal={true}
//               scrollEventThrottle={16}
//               pagingEnabled={true}
//               showsHorizontalScrollIndicator={false}
//               onMomentumScrollEnd={event => {
//                 // console.log("here");
//                 // console.log("***");
//                 // console.log(event.nativeEvent.contentOffset.x);
//                 // console.log("screenWidth " + event.nativeEvent.contentOffset.x/screenWidth);
//                 // // console.log(event.nativeEvent.contentOffset.y);
//                 // console.log("***");
//                 console.log(
//                   '' +
//                     (Math.round(
//                       event.nativeEvent.contentOffset.x / screenWidth,
//                     ) +
//                       1),
//                 );
//                 setCurrentIndex(
//                   Math.round(event.nativeEvent.contentOffset.x / screenWidth) +
//                     1,
//                 );
//               }}>
//               {infoArr.map(obj => {
//                 console.log('currentIndex ' + currentIndex + ' ' + obj);
//                 console.log('currentIndex ' + (currentIndex == obj));
//                 return (
//                   <View style={styles.container}>
//                     <Image
//                       style={{
//                         width: widthPercentageToDP('90%'),
//                         height: heightPercentageToDP('30%'),
//                       }}
//                       source={obj.image}
//                     />

//                     <Text
//                       style={{
//                         fontSize: 22,
//                         color: 'BLACK',
//                         fontWeight: 'bold',
//                         alignSelf: 'flex-start',
//                         marginTop: 100,
//                         opacity: 0.9,
//                       }}>
//                       {obj.title}
//                     </Text>
//                     <Text
//                       style={{
//                         fontSize: convertFontScale(15),
//                         color: '#A8A8A8',
//                         alignSelf: 'flex-start',
//                         marginTop: convertFontScale(4),
//                       }}>
//                       {obj.description}
//                     </Text>
//                   </View>
//                 );
//               })}
//             </ScrollView>
//           </View>

//           {/* page controller  */}
//           <View
//             style={{
//               alignSelf: 'flex-start',
//               flexDirection: 'row',
//               paddingHorizontal: convertFontScale(22),
//             }}>
//             <View
//               style={{
//                 backgroundColor:
//                   currentIndex == 1 ? color.bgColor[0] : color.bgColor[1],
//                 height: convertFontScale(4),
//                 width:
//                   currentIndex == 1
//                     ? convertFontScale(26)
//                     : convertFontScale(20),
//                 borderRadius: convertFontScale(3),
//               }}
//             />
//             <View
//               style={{
//                 backgroundColor:
//                   currentIndex == 2 ? color.bgColor[0] : color.bgColor[1],
//                 height: convertFontScale(4),
//                 width:
//                   currentIndex == 2
//                     ? convertFontScale(26)
//                     : convertFontScale(20),
//                 borderRadius: convertFontScale(3),
//                 marginLeft: convertFontScale(6),
//               }}
//             />
//             <View
//               style={{
//                 backgroundColor:
//                   currentIndex == 3 ? color.bgColor[0] : color.bgColor[1],
//                 height: convertFontScale(4),
//                 width:
//                   currentIndex == 3
//                     ? convertFontScale(26)
//                     : convertFontScale(20),
//                 borderRadius: convertFontScale(3),
//                 marginLeft: convertFontScale(6),
//               }}
//             />
//             <View
//               style={{
//                 backgroundColor:
//                   currentIndex == 4 ? color.bgColor[0] : color.bgColor[1],
//                 height: convertFontScale(4),
//                 width:
//                   currentIndex == 4
//                     ? convertFontScale(26)
//                     : convertFontScale(20),
//                 borderRadius: convertFontScale(3),
//                 marginLeft: convertFontScale(6),
//               }}
//             />
//           </View>
//           {/* footer */}
//         </View>

//         <View
//           style={{
//             position: 'absolute',
//             bottom: convertFontScale(50),
//             alignItems: 'center',
//             alignSelf: 'center',
//             width: '100%',
//           }}>
//           <TouchableOpacity
//             style={{
//               alignItems: 'center',
//               alignSelf: 'center',
//               justifyContent: 'center',
//               borderRadius: convertFontScale(6),
//               width: widthPercentageToDP('80%'),
//               height: convertFontScale(48),
//               backgroundColor: color.bgColor[0],
//             }}
//             onPress={async () => {
//               props.navigation.replace(Message.screen_location_perm);
//             }}>
//             <Text
//               style={{
//                 fontSize: convertFontScale(15),
//                 color: color.WHITE,
//                 fontWeight: 'bold',
//               }}>
//               {Message.skip_intro}
//             </Text>
//           </TouchableOpacity>
//         </View>
//       </SafeAreaView>
//     );
//   }
// }
// const styles = StyleSheet.create({
//   MainContainer: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     paddingTop: Platform.OS === 'ios' ? 20 : 0,
//   },

//   SplashScreen_RootView: {
//     justifyContent: 'center',
//     flex: 1,
//     margin: 10,
//     position: 'absolute',
//     width: '100%',
//     height: '100%',
//   },

//   SplashScreen_ChildView: {
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#000',
//     flex: 1,
//   },
// });
